# 2D Frames Registration

Fiji scripts for the registration of 2D multichannels movie using the bUnwarpJ plugin.

- stabilization1.py: perform a registration and save the transformed matrices into txt files.
- stabilization2.py: use the transformed matrices from txt files to do the registration.
- utils.py: functions support for running the scripts.

A user guide can be found [here](https://docs.google.com/document/d/19N3uftv3SbeyYzpvcVhuEseGRmIYEGAyx9o5kMJDZ9o/edit?usp=sharing).
