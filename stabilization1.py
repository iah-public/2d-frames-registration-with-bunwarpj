"""
Movie stabilization
Author: Son Phan, IAH Pasteur
-----------------------------
Input: 
    2D multichannels image stack XYCT
Output: 
    2D multichannels image stack XYCT after stabilization

Aligning two consecutive image frames using bUnwarpJ starting from a referenced 
frame. Some channels are used to compute the transformation, the transformation is 
then applied to the selected channels. The plugin bUnwarpJ is used to compute the transformation.
"""

import sys, os, re, inspect
from ij import IJ
from ij.process import ImageConverter
from ij.plugin import Concatenator, RGBStackMerge, ZProjector
from bunwarpj import bUnwarpJ_
from fiji.util.gui import GenericDialogPlus

# get the directory of current file and append to system path
# NOTE: os.get_cwd() doesn't work in Jython since the file is run within Fiji env
current_file_path = os.path.abspath(inspect.getsourcefile(lambda:0))
current_directory_path = os.path.dirname(current_file_path) 
sys.path.append(current_directory_path)
from utils import splitChannels, prepareRegisteredFrame

def getOptions():
    """Define UI input. The UI only works with Fiji.
    """
    gd = GenericDialogPlus("Movie stabilization")
    gd.addFileField("Input movie",None)
    gd.addDirectoryField("Registered files output",None)
    gd.addStringField("Reference channels","1")
    gd.addToSameRow()
    gd.addMessage("Separated by commas e.g. 1, 2, 3")
    gd.addStringField("Applied channels","1")
    gd.addToSameRow()
    gd.addMessage("Separated by commas e.g. 1, 2, 3")
    gd.addNumericField("Referenced frame",1,0)
    gd.addCheckbox("Output as individual channel",False)
    gd.addCheckbox("Verbose",False)
    gd.addMessage("---------bUnwarpJ parameters---------")
    registration_modes = ["fast", "accurate", "mono"]
    gd.addChoice("Registration mode",registration_modes,registration_modes[2])
    gd.addNumericField("Subsample factor",0,0)
    init_deformation_modes = ["very coarse", "coarse", "fine", "very fine"]
    gd.addChoice("Initial deformation",init_deformation_modes,init_deformation_modes[1])
    final_deformation_modes = init_deformation_modes + ["super fine"]
    gd.addChoice("Final deformation",final_deformation_modes,final_deformation_modes[2])
    gd.addNumericField("Divergence weight",0.,2)
    gd.addNumericField("Curl weight",0.,2)
    gd.addNumericField("Landmark weight",0.,2)
    gd.addNumericField("Image weight",1.,2)
    gd.addNumericField("Consistency weight",10.,2)
    gd.addNumericField("Stop threshold",0.01,2)
    
    gd.showDialog()

    filepath = gd.getNextString()
    outpath = gd.getNextString()
    refchannels = gd.getNextString()
    appliedchannels = gd.getNextString()
    refframe = gd.getNextNumber()
    individual_output = gd.getNextBoolean()
    verbose = gd.getNextBoolean()
    mode = gd.getNextChoiceIndex()
    sub_sampling = gd.getNextNumber()
    min_scale = gd.getNextChoiceIndex()
    max_scale = gd.getNextChoiceIndex()
    div_w = gd.getNextNumber()
    curl_w = gd.getNextNumber()
    landmark_w = gd.getNextNumber()
    img_w = gd.getNextNumber()
    consistent_w = gd.getNextNumber()
    stop_thres = gd.getNextNumber()
    
    return filepath, outpath, refchannels, appliedchannels, refframe, individual_output, verbose, mode, sub_sampling, min_scale, max_scale, div_w, curl_w, landmark_w, img_w, consistent_w, stop_thres

# open dialog and import input
options = getOptions()
filepath, outpath, refchannels, appliedchannels, refframe, individual_output, verbose, mode, sub_sampling, min_scale, max_scale, div_w, curl_w, landmark_w, img_w, consistent_w, stop_thres = options

# checking input
assert filepath != "", "Movie path is empty."
assert outpath != "", "directory path for registered files is empty."

# preprocess input
filename = os.path.basename(filepath).split(".")[0]
referenced_channel_id = [int(item) for item in re.findall(r"\d+",refchannels)]
applied_channels_id = [int(item) for item in re.findall(r"\d+",appliedchannels)]
refframe = int(refframe)
sub_sampling = int(sub_sampling)

print "---Input movie:---"
print filename
print "---Movie parameters:---"
print "Referenced channels:", referenced_channel_id
print "Applied channels:", applied_channels_id
print "Referenced frame:", refframe
print "Output as individual channel:", individual_output
print "Verbose:", verbose
print "---bUnwarpJ parameters:---"
print "Registration mode:", mode
print "Subsample:", sub_sampling
print "Initial deformation:", min_scale
print "Final deformation:", max_scale
print "Divergence weight:", div_w
print "Curl weight:",curl_w
print "Landmark weight:", landmark_w
print "Image weight:", img_w
print "Consistency weight:", consistent_w
print "Stop threshold:", stop_thres

# additional bUnwarpJ parameters
target_mask = None
source_mask = None

# open input movie
img = IJ.openImage(filepath)

nframes = img.getNFrames()
print 'nb frames: {}'.format(nframes)
assert refframe <= nframes, "Referenced frame must be <= {}".format(nframes)

nchannels = img.getNChannels()
assert max(referenced_channel_id) <= nchannels, "Referenced channel ID must be <= {}".format(nchannels)
assert max(applied_channels_id) <= nchannels, "Applied channel ID must be <= {}".format(nchannels)

# get individual channels
channels = splitChannels(img)

print "---Processing---"

# add referenced frame from referenced channels to registered bunwarpj stack 
registered_bunwarpj = prepareRegisteredFrame(refframe, referenced_channel_id, channels)
registered_bunwarpj.setTitle("Registering: init")
if verbose == True:
    registered_bunwarpj.show()
    avg = ZProjector.run(registered_bunwarpj,"avg");
    avg.setTitle("Averaging: init")
    avg.show()

# add referenced frame from applied channels to registered stack
registered_channels = []
for ic in applied_channels_id:
    channels[ic-1].setSlice(refframe)
    sc = channels[ic-1].crop()
    ImageConverter(sc).convertToGray16()
    registered_channels.append(sc)

# go from reference frame to last frame
print "From referenced frame {} to last frame {}".format(refframe,nframes)
for iframe in range(refframe+1,nframes+1):
    
    print "Aligning frame {}".format(iframe)
    
    # target slice
    registered_bunwarpj.setSlice(iframe-refframe)
    target_bunwarpj = registered_bunwarpj.crop()
    target_bunwarpj.setTitle("target")
        
    # source slice
    source_bunwarpj = prepareRegisteredFrame(iframe, referenced_channel_id, channels)
    source_bunwarpj.setTitle("source")
    
    # compute transformation
    transformation = bUnwarpJ_().computeTransformationBatch(target_bunwarpj, source_bunwarpj, target_mask, source_mask, 
                                              mode, sub_sampling, min_scale, max_scale,
                                              div_w, curl_w, landmark_w, img_w, consistent_w,
                                              stop_thres)
    
    # save transformation
    transformed_file = os.path.join(outpath,"ref{}_frame{}.txt".format(refframe,iframe))
    transformation.saveDirectTransformation(transformed_file)
    
    # apply transformation to bunwarpj frames
    transformed_bunwarpj = source_bunwarpj.duplicate()
    bUnwarpJ_().applyTransformToSource(transformed_file,target_bunwarpj,transformed_bunwarpj)
    if len(referenced_channel_id)==1:
        ImageConverter(transformed_bunwarpj).convertToGray16()
    registered_bunwarpj = Concatenator.run(registered_bunwarpj, transformed_bunwarpj.duplicate())
    if verbose == True:
        registered_bunwarpj.setTitle("Registering: frame {}".format(iframe))
        registered_bunwarpj.show()
        avg.changes = False
        avg.close()
        avg = ZProjector.run(registered_bunwarpj,"avg");
        avg.setTitle("Averaging: frame {}".format(iframe))
        avg.show()
    
    # apply transformation to raw frames
    for i in range(len(applied_channels_id)):
        
        ic = applied_channels_id[i]
        
        registered_channels[i].setSlice(iframe - refframe)
        target_sc = registered_channels[i].crop()
        
        channels[ic-1].setSlice(iframe)
        source_sc = channels[ic-1].crop()
        
        bUnwarpJ_().applyTransformToSource(transformed_file,target_sc,source_sc)
        ImageConverter(source_sc).convertToGray16()
        
        registered_channels[i] = Concatenator.run(registered_channels[i], source_sc)        
 
# go from referenced frame to first frame
print "From reference frame {} to first frame".format(refframe)
for iframe in range(refframe-1,0,-1):
    
    print "Aligning frame {}".format(iframe)
    
    # target slice
    registered_bunwarpj.setSlice(1)
    target_bunwarpj = registered_bunwarpj.crop()
    target_bunwarpj.setTitle("target")
        
    # source slice
    source_bunwarpj = prepareRegisteredFrame(iframe, referenced_channel_id, channels)
    source_bunwarpj.setTitle("source")
    
    # compute transformation
    transformation = bUnwarpJ_().computeTransformationBatch(target_bunwarpj, source_bunwarpj, target_mask, source_mask, 
                                              mode, sub_sampling, min_scale, max_scale,
                                              div_w, curl_w, landmark_w, img_w, consistent_w,
                                              stop_thres)
    
    # save transformation
    transformed_file = os.path.join(outpath,"ref{}_frame{}.txt".format(refframe,iframe))
    transformation.saveDirectTransformation(transformed_file)
    
    # apply transformation to bunwarpj frames
    transformed_bunwarpj = source_bunwarpj.duplicate()
    bUnwarpJ_().applyTransformToSource(transformed_file,target_bunwarpj,transformed_bunwarpj)
    if len(referenced_channel_id)==1:
        ImageConverter(transformed_bunwarpj).convertToGray16()
    registered_bunwarpj = Concatenator.run(transformed_bunwarpj.duplicate(), registered_bunwarpj)
    if verbose == True:
        registered_bunwarpj.setTitle("Registering: frame {}".format(iframe))
        registered_bunwarpj.show()
        avg.changes = False
        avg.close()
        avg = ZProjector.run(registered_bunwarpj,"avg");
        avg.setTitle("Averaging: frame {}".format(iframe))
        avg.show()
    
    # apply transformation to raw frames
    for i in range(len(applied_channels_id)):
        
        ic = applied_channels_id[i]
        
        registered_channels[i].setSlice(1)
        target_sc = registered_channels[i].crop()
        
        channels[ic-1].setSlice(iframe)
        source_sc = channels[ic-1].crop()
        
        bUnwarpJ_().applyTransformToSource(transformed_file,target_sc,source_sc)
        ImageConverter(source_sc).convertToGray16()
        
        registered_channels[i] = Concatenator.run(source_sc, registered_channels[i])        

# evaluate results
print "---Done---"

if (individual_output == True) | (len(applied_channels_id)==1):
    for i in range(len(applied_channels_id)):
        registered_channels[i].setTitle("Registered_{}_C{}".format(filename,applied_channels_id[i]))
        registered_channels[i].show()
else:
    combined_registered_channels = RGBStackMerge.mergeChannels(registered_channels, True)
    combined_registered_channels.setTitle("Registered_{}".format(filename))
    combined_registered_channels.show()

registered_bunwarpj.setTitle("Registered_bUnwarpJ")
registered_bunwarpj.show()

if verbose == True:
    avg.changes = False
    avg.close()
avg = ZProjector.run(registered_bunwarpj,"avg")
avg.setTitle("Registered_{}_AveragedRGB".format(filename))
avg.show()
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



























