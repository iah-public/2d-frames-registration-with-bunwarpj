"""
Movie stabilization
Author: Son Phan, IAH Pasteur
-----------------------------
Input: 
    2D multichannels image stack XYCT
    Transformation files
Output:
    2D multichannels image stack XYCT after stabilization
    
Applying transformation from files to input movie.
"""

import sys, os, glob, re, inspect
from ij import IJ
from ij.process import ImageConverter
from ij.plugin import Concatenator, RGBStackMerge
from bunwarpj import bUnwarpJ_
from fiji.util.gui import GenericDialogPlus

# get the directory of current file and append to system path
# NOTE: os.get_cwd() doesn't work in Jython since the file is run within Fiji env
current_file_path = os.path.abspath(inspect.getsourcefile(lambda:0))
current_directory_path = os.path.dirname(current_file_path) 
sys.path.append(current_directory_path)
from utils import splitChannels, getTransformedFile

def getOptions():
    """Define UI input. The UI only works with Fiji.
    """
    gd = GenericDialogPlus("Movie stabilization")
    gd.addFileField("Input movie",None)
    gd.addDirectoryField("Directory of registered files",None)
    
    gd.showDialog()

    filepath = gd.getNextString()
    outpath = gd.getNextString()
    
    return filepath, outpath

# open dialog and import input
options = getOptions()
filepath, outpath = options
assert filepath != "", "Movie path is empty."
assert outpath != "", "Directory path for registered files is empty."

# open input movie
img = IJ.openImage(filepath)
nframes = img.getNFrames()
nchannels = img.getNChannels()

# preprocess input
filename = os.path.basename(filepath).split(".")[0]
alltxtfiles = glob.glob(os.path.join(outpath,"*.txt"))
transformed_files = filter(lambda x: len(re.findall(r"ref\d+_frame\d+.txt",x))==1, alltxtfiles)
assert len(transformed_files)>0, "Transformation files are not found."
refframe = int(re.findall(r"\d+",transformed_files[0])[0])
assert refframe <= nframes, "Referenced frame must be <= {}".format(nframes)

print "Input movie:", filename
print "{} transformation files were found.".format(len(transformed_files))
print "Referenced frame:", refframe

# get individual channels
channels = splitChannels(img)

print "---Processing---"

# add referenced frame at each channel to registered stack
registered_channels = []
for ic in range(1,nchannels+1):
    channels[ic-1].setSlice(refframe)
    sc = channels[ic-1].crop()
    ImageConverter(sc).convertToGray16()
    registered_channels.append(sc)
    
# go from reference frame to last frame
print "From referenced frame {} to last frame {}".format(refframe,nframes)
for iframe in range(refframe+1,nframes+1):
    
    print "Aligning frame {}".format(iframe)
    
    transformed_file = getTransformedFile(iframe,refframe,transformed_files)
    
    # apply transformation to raw frames
    for ic in range(1,nchannels+1):
        registered_channels[ic-1].setSlice(iframe - refframe)
        target_sc = registered_channels[ic-1].crop()
        
        channels[ic-1].setSlice(iframe)
        source_sc = channels[ic-1].crop()
        
        bUnwarpJ_().applyTransformToSource(transformed_file,target_sc,source_sc)
        ImageConverter(source_sc).convertToGray16()
        
        registered_channels[ic-1] = Concatenator.run(registered_channels[ic-1], source_sc)
        
# go from referenced frame to first frame
print "From reference frame {} to first frame".format(refframe)
for iframe in range(refframe-1,0,-1):
    
    print "Aligning frame {}".format(iframe)
    
    transformed_file = getTransformedFile(iframe,refframe,transformed_files)
    
    # apply transformation to raw frames
    for ic in range(1,nchannels+1):
        registered_channels[ic-1].setSlice(1)
        target_sc = registered_channels[ic-1].crop()
        
        channels[ic-1].setSlice(iframe)
        source_sc = channels[ic-1].crop()
        
        bUnwarpJ_().applyTransformToSource(transformed_file,target_sc,source_sc)
        ImageConverter(source_sc).convertToGray16()
        
        registered_channels[ic-1] = Concatenator.run(source_sc, registered_channels[ic-1])  
        
# evaluate results
print "---Done---"

if nchannels==1:
    registered_channels[0].setTitle("Registered_{}".format(filename))
    registered_channels[0].show()
else:
    combined_registered_channels = RGBStackMerge.mergeChannels(registered_channels, True)
    combined_registered_channels.setTitle("Registered_{}".format(filename))
    combined_registered_channels.show()




































