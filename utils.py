"""
Movie stabilization
Author: Son Phan, IAH Pasteur
-----------------------------
Support functions.
"""
import os
from ij import IJ, ImagePlus
from ij.process import ImageConverter
from ij.plugin import ChannelSplitter, RGBStackConverter, RGBStackMerge

def splitChannels(img):
    """Split image into separated channels
    
    Args:
        img: image stack (2D + Channels + Time)
    
    Returns:
        list of individual channel stacks (2D + Time)

    """
    
    nchannels = img.getNChannels()
    channels = []
    for ic in range(1, nchannels+1):
        cstack = ChannelSplitter().getChannel(img,ic).duplicate()
        channels.append(ImagePlus("C{}".format(ic), cstack))
        
    return channels
    

def prepareRegisteredFrame(slice_number, referenced_channel_id, channels):
    """Prepare frame used for bUnwarpJ. Frame will be equalized its contrast.
    Frame with multiple channels will be converted to RGB.
    
    Args:
        slice_number (int): slice ID
        referenced_channel_id (list [int]): list of channel IDs
        channels: list of individual channel stacks (2D + Time)
        
    Returns:
        Gray or RGB frame.
    
    """
    
    subchannels = []
    for ic in referenced_channel_id:
        channels[ic-1].setSlice(slice_number)
        sc = channels[ic-1].crop()
        IJ.run(sc, "Enhance Contrast...", "saturate=0 equalize");
        ImageConverter(sc).convertToGray16()
        subchannels.append(sc)
    
    if len(subchannels)==1:
        return subchannels[0]
    else:
        cc = RGBStackMerge.mergeChannels(subchannels, True)
        RGBStackConverter().convertToRGB(cc)
        return cc
    
def getTransformedFile(iframe,refframe,transformed_files):
    """Get corresponding transformation file to given iframe from list of transformed_files.
    Args:
        iframe (int): frame ID
        refframe (int): referenced frame ID
        transformed_files (list[str]): list of transformed file paths
    """
    res = filter(lambda x: os.path.basename(x) == "ref{}_frame{}.txt".format(refframe,iframe), transformed_files)
    try:
        return res[0]
    except:
        raise Exception("Transformation files of frame {} is not found.".format(iframe))
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
